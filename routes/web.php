<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\HomePageController::class, 'index']);

Route::get('/san-pham/{id}', [\App\Http\Controllers\HomePageController::class, 'viewSanPham']);
Route::get('/danh-muc/{id}', [\App\Http\Controllers\HomePageController::class, 'viewDanhMuc']);
Route::get('/cart', [\App\Http\Controllers\ChiTietDonHangController::class, 'index']);

Route::get('/cart/data', [\App\Http\Controllers\ChiTietDonHangController::class, 'dataCart']);
Route::get('/hoa-don/data', [\App\Http\Controllers\ChiTietDonHangController::class, 'getHoaDon']);
Route::get('/list-hoa-don', [\App\Http\Controllers\ChiTietDonHangController::class, 'listHoaDon']);

Route::post('/add-to-cart-update', [\App\Http\Controllers\ChiTietDonHangController::class, 'addToCartUpdate']);
Route::post('/remove-cart', [\App\Http\Controllers\ChiTietDonHangController::class, 'removeCart']);

Route::post('/add-to-cart', [\App\Http\Controllers\ChiTietDonHangController::class, 'addToCart']);

Route::post('/thanh-toan', [\App\Http\Controllers\ChiTietDonHangController::class , 'thanhToan']);

Route::get('/test', [\App\Http\Controllers\TestController::class, 'test']);

Route::group(['prefix' => '/admin'], function() {
    Route::get('/',[\App\Http\Controllers\TestController::class, 'index']);
    Route::group(['prefix' => '/danh-muc'], function() {
        Route::get('/index', [\App\Http\Controllers\DanhMucSanPhamController::class, 'index']);
        Route::post('/index', [\App\Http\Controllers\DanhMucSanPhamController::class, 'store']);

        Route::get('/edit/{id}', [\App\Http\Controllers\DanhMucSanPhamController::class, 'edit']);
        Route::get('/delete/{id}', [\App\Http\Controllers\DanhMucSanPhamController::class, 'delete']);

        Route::post('/update', [\App\Http\Controllers\DanhMucSanPhamController::class, 'update']);

    });

    Route::group(['prefix' => '/san-pham'], function() {
        Route::get('/index', [\App\Http\Controllers\SanPhamController::class, 'index']);
        Route::get('/list', [\App\Http\Controllers\SanPhamController::class, 'list']);

        Route::get('/loadData', [\App\Http\Controllers\SanPhamController::class, 'loadData']);

        Route::post('/create', [\App\Http\Controllers\SanPhamController::class, 'store']);
        Route::post('/update', [\App\Http\Controllers\SanPhamController::class, 'update']);

        Route::get('/edit/{id}', [\App\Http\Controllers\SanPhamController::class, 'edit']);
        Route::get('/delete/{id}', [\App\Http\Controllers\SanPhamController::class, 'delete']);

        Route::post('/search', [\App\Http\Controllers\SanPhamController::class, 'search']);
    });

    Route::group(['prefix' => '/nhap-kho'], function() {
        Route::get('/index', [\App\Http\Controllers\KhoHangController::class, 'index']);
        Route::get('/list', [\App\Http\Controllers\KhoHangController::class, 'list']);

        Route::get('/loadData', [\App\Http\Controllers\KhoHangController::class, 'loadData']);
        Route::get('/loadKho', [\App\Http\Controllers\KhoHangController::class, 'loadKho']);
        Route::get('/add/{id}', [\App\Http\Controllers\KhoHangController::class, 'store']);

        Route::get('/remove/{id}', [\App\Http\Controllers\KhoHangController::class, 'destroy']);
        Route::post('/update', [\App\Http\Controllers\KhoHangController::class, 'update']);

        Route::get('/create', [\App\Http\Controllers\KhoHangController::class, 'create']);
    });

    Route::group(['prefix' => '/cau-hinh'], function() {
        Route::get('/', [\App\Http\Controllers\ConfigController::class, 'index']);
        Route::post('/', [\App\Http\Controllers\ConfigController::class, 'store']);
    });
});

Route::group(['prefix' => '/agent'], function() {
    Route::get('/product', [\App\Http\Controllers\SanPhamController::class, 'viewProduct']);
});
Route::get('/agent/register', [\App\Http\Controllers\AgentController::class, 'register']);
Route::post('/agent/register', [\App\Http\Controllers\AgentController::class, 'registerAction']);
Route::get('/agent/login', [\App\Http\Controllers\AgentController::class, 'login']);
Route::get('/agent/login-addtocart', [\App\Http\Controllers\AgentController::class, 'login_addtocart']);
Route::get('/agent/logout', [\App\Http\Controllers\AgentController::class, 'logout']);
Route::post('/agent/login', [\App\Http\Controllers\AgentController::class, 'loginAction']);
