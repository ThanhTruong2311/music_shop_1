<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <!-- main menu header-->
    <!-- / main menu header-->
    <!-- main menu content-->
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" nav-item"><a href="/"><i class="feather icon-home"></i><span class="menu-title" data-i18n="">Home</span></a>
            </li>
            <li class=" nav-item"><a href="/admin/danh-muc/index"><i class="fa-brands fa-audible"></i><span class="menu-title" data-i18n="">Quản Lý Danh Mục</span></a>
            </li>
            <li class=" nav-item"><a href="#"><i class="fa-solid fa-diagram-project"></i><span class="menu-title" data-i18n="">Quản Lý Sản Phẩm</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="/admin/san-pham/index">Thêm Mới Sản Phẩm</a>
                    </li>
                    <li><a class="menu-item" href="/admin/san-pham/list">Danh Sách Sản Phẩm</a>
                    </li>
                </ul>
            </li>
            <li class=" nav-item"><a><i class="fa-solid fa-warehouse"></i><span class="menu-title" data-i18n="">Quản Lý Nhập Kho</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="/admin/nhap-kho/index">Nhập Kho</a>
                    </li>
                    <li><a class="menu-item" href="/admin/nhap-kho/list">Danh Sách Nhập Kho</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- /main menu content-->
    <!-- main menu footer-->
    <!-- main menu footer-->
</div>
