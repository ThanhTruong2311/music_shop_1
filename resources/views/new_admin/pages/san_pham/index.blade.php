@extends('new_admin.master')
@section('title')
    <h3>Quản Lý Sản Phẩm</h3>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <form id="formCreate" action="/admin/san-pham/create" method="POST" enctype="multipart/form-data">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Thêm Mới Sản Phẩm</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <div class="row">
                                    @csrf
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label>Tên Sản Phẩm</label>
                                                    <input type="text" class="form-control" name="ten_san_pham" placeholder="Nhập vào tên sản phẩm">
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label>Slug Sản Phẩm</label>
                                                    <input type="text" class="form-control" name="slug_san_pham" placeholder="Nhập vào slug sản phẩm">
                                                </fieldset>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <fieldset class="form-group">
                                                    <label>Giá Bán</label>
                                                    <input type="number" class="form-control" name="gia_ban" placeholder="Nhập vào giá bán">
                                                </fieldset>
                                            </div>
                                            <div class="col-md-4">
                                                <fieldset class="form-group">
                                                    <label>Giá Khuyến Mãi</label>
                                                    <input type="number" class="form-control" name="gia_khuyen_mai" placeholder="Nhập vào giá khuyến mãi">
                                                </fieldset>
                                            </div>
                                            <div class="col-md-4">
                                                <fieldset class="form-group">
                                                    <label>Ảnh Đại Diện</label>
                                                    <input type="file" name="anh_dai_dien" class="form-control">
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <fieldset class="form-group">
                                                    <label for="placeTextarea">Mô Tả Ngắn</label>
                                                    <textarea class="form-control" name="mo_ta_ngan" cols="30" rows="5" placeholder="Nhập vào mô tả ngắn"></textarea>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="position-relative form-group">
                                            <label>Mô Tả Chi Tiết</label>
                                            <textarea name="mo_ta_chi_tiet"  placeholder="Nhập vào mô tả chi tiết" type="text" class="form-control"></textarea>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label>Danh Mục</label>
                                                    <select name="id_danh_muc" class="custom-select block">
                                                        @foreach ($list_danh_muc as $value)
                                                            <option value={{$value->id}}> {{ $value->ten_danh_muc }} </option>
                                                        @endforeach
                                                    </select>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label>Danh Mục</label>
                                                    <select name="is_open" class="custom-select block">
                                                        <option value=1>Hiển Thị</option>
                                                        <option value=0>Tạm tắt</option>
                                                    </select>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <button type="submit" class="mt-1 btn btn-primary">Thêm Mới Sản Phẩm</button>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
@section('js')
<script src="https://cdn.ckeditor.com/4.18.0/standard/ckeditor.js"></script>
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
<script>
    CKEDITOR.replace('mo_ta_chi_tiet');
</script>
@endsection
