@extends('new_admin.master')
@section('title')
    <h3>Cập Nhật Sản Phẩm</h3>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <form id="formCreate" action="/admin/san-pham/update" method="POST" enctype="multipart/form-data">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Cập Nhật Sản Phẩm</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <div class="row">
                                    @csrf
                                    <input type="hidden" name="id" value="{{ $san_pham->id }}">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label>Tên Sản Phẩm</label>
                                                    <input type="text" class="form-control" name="ten_san_pham" value="{{ $san_pham->ten_san_pham }}" >
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label>Slug Sản Phẩm</label>
                                                    <input type="text" class="form-control" name="slug_san_pham" value="{{ $san_pham->slug_san_pham }}">
                                                </fieldset>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <fieldset class="form-group">
                                                    <label>Giá Bán</label>
                                                    <input type="number" class="form-control" name="gia_ban" value="{{ $san_pham->gia_ban }}">
                                                </fieldset>
                                            </div>
                                            <div class="col-md-4">
                                                <fieldset class="form-group">
                                                    <label>Giá Khuyến Mãi</label>
                                                    <input type="number" class="form-control" name="gia_khuyen_mai" value="{{ $san_pham->gia_khuyen_mai }}" >
                                                </fieldset>
                                            </div>
                                            <div class="col-md-4">
                                                <fieldset class="form-group">
                                                    <label>Ảnh Đại Diện</label>
                                                    <input type="file" name="anh_dai_dien" class="form-control" value="{{ $san_pham->anh_dai_dien }}">
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <fieldset class="form-group">
                                                    <label for="placeTextarea">Mô Tả Ngắn</label>
                                                    <textarea class="form-control" name="mo_ta_ngan" cols="30" rows="5" >{{ $san_pham->mo_ta_ngan }}</textarea>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="position-relative form-group">
                                            <label>Mô Tả Chi Tiết</label>
                                            <textarea name="mo_ta_chi_tiet" type="text" class="form-control" >{{ $san_pham->mo_ta_chi_tiet }}</textarea>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label>Danh Mục</label>
                                                    <select name="id_danh_muc" class="custom-select block">
                                                        @foreach ($list_danh_muc as $value)
                                                            <option value="{{ $value->id }}" {{$value->id = $san_pham->id_danh_muc ? 'selected' : '' }}>{{ $value->ten_danh_muc }}</option>
                                                        @endforeach
                                                    </select>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label>Trang Thái</label>
                                                    <select name="is_open" class="custom-select block">
                                                        <option value=1 {{ $san_pham->is_open == 1 ? 'selected' : '' }}>Hiển Thị</option>
                                                        <option value=0 {{ $san_pham->is_open == 0 ? 'selected' : '' }}>Tạm Tắt</option>
                                                    </select>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <button type="submit" class="mt-1 btn btn-primary">Cập Nhật Sản Phẩm</button>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
@section('js')
<script src="https://cdn.ckeditor.com/4.18.0/standard/ckeditor.js"></script>
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
<script>
    CKEDITOR.replace('mo_ta_chi_tiet');
</script>
@endsection
