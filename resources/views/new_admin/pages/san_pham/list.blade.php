@extends('new_admin.master')
@section('title')
    <h3>Quản Lý Sản Phẩm</h3>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Danh Sách Sản Phẩm</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="table-responsive">
                        <table class="table table-bordered mb-0" id="tableSanPham">
                            <thead>
                                <tr>
                                    <th class="text-nowrap text-center">#</th>
                                    <th class="text-nowrap text-center">Tên Sản Phẩm</th>
                                    <th class="text-nowrap text-center">Giá Bán</th>
                                    <th class="text-nowrap text-center">Giá Khuyến Mãi</th>
                                    <th class="text-nowrap text-center">Tình Trạng</th>
                                    <th class="text-nowrap text-center">Danh Mục</th>
                                    <th class="text-nowrap text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $key=>$value)
                                    <tr class="align-middle">
                                        <th class="text-nowrap text-center">{{ $key + 1 }}</th>
                                        <td class="text-nowrap text-center">{{ $value->ten_san_pham }}</td>
                                        <td class="text-nowrap text-center">{{ $value->gia_ban }}</td>
                                        <td class="text-nowrap text-center">{{ $value->gia_khuyen_mai }}</td>
                                        <td class="text-nowrap text-center">
                                            @if ($value->is_open)
                                                    <button class="btn btn-primary" >Hiển Thị</button>
                                                @else
                                                    <button class="btn btn-danger">Tạm Tắt</button>
                                                @endif
                                        </td>
                                        <td class="text-nowrap text-center">{{ $value->ten_danh_muc }}</td>
                                        <td class="text-nowrap text-center">
                                            <a class="btn btn-primary" href="/admin/san-pham/edit/{{ $value->id }}" >Edit</a>
                                            <a class="btn btn-danger" href="/admin/san-pham/delete/{{ $value->id }}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script src="https://cdn.ckeditor.com/4.18.0/standard/ckeditor.js"></script>
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
<script>
    CKEDITOR.replace('mo_ta_chi_tiet');
</script>
@endsection
