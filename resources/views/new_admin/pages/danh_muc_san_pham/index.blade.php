@extends('new_admin.master')
@section('title')
    <h3>Quản Lý Danh Mục</h3>
@endsection
@section('content')
<div id="app">
    <div class="row">
        <div class="col-md-4">
            <div class="card" style="height: auto">
                <div class="card-header">
                    <h4 class="card-title" id="basic-layout-colored-form-control">Thêm mới danh mục</h4>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <form class="form" action="/admin/danh-muc/index" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="userinput1">Tên Danh Mục</label>
                                            <input type="text" class="form-control"  name="ten_danh_muc">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="userinput3">Hình Ảnh</label>
                                            <input type="file" name="hinh_anh"  class="form-control" >
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-12">
                                        <label for="issueinput6">Danh Mục Cha</label>
                                        <select name="id_danh_muc_cha" class="form-control" >
                                            <option value="">Root</option>
                                            @foreach ($danhMuc as $value)
                                                <option value="{{ $value->id }}">{{ $value->ten_danh_muc }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-12">
                                        <label for="issueinput6">Tình Trạng</label>
                                        <select name="is_open" class="form-control" >
                                            <option value="1" >Hiển Thị</option>
                                            <option value="0">Tạm Tắt</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-actions right">
                                <button type="submit" class="btn btn-primary">Thêm mới Danh Mục
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Danh sách danh mục</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                        <tr class="text-center">
                                            <th>#</th>
                                            <th>Tên Danh Mục</th>
                                            <th>Danh Mục Cha</th>
                                            <th>Tình Trạng</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $key=>$value)
                                        <tr>
                                            <th class="text-center align-middle">{{ $key + 1 }}</th>
                                            <td class="align-middle">{{ $value->ten_danh_muc }}</td>
                                            <td class="text-center align-middle">{{ $value->danh_muc_cha }}</td>
                                            <td class="text-center">
                                                @if ($value->is_open)
                                                    <button class="btn btn-primary" >Hiển Thị</button>
                                                @else
                                                    <button class="btn btn-danger">Tạm Tắt</button>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a class="btn btn-primary" href="/admin/danh-muc/edit/{{ $value->id }}" >Edit</a>
                                                <a class="btn btn-danger" href="/admin/danh-muc/delete/{{ $value->id }}">Delete</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('js')
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
<script>
    $('.lfm').filemanager('image');
</script>
@endsection
