@extends('new_admin.master')
@section('title')
    <h3>Danh Sách Nhập Kho</h3>
@endsection
@section('content')
<div id="app" class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" style="height: auto">
                <h4 class="card-title">Nhập Kho Sản Phẩm</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                        <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                        <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <table class="table table-bordered mb-0">
                        <thead>
                            <tr class="text-center">
                                <th class="text-center">#</th>
                                <th class="text-center">Ngày Nhập</th>
                                <th class="text-center">Tên Sản Phẩm</th>
                                <th class="text-center">Số Lượng</th>
                                <th class="text-center">Đơn Giá</th>
                                <th class="text-center">Thành Tiền</th>
                                {{-- <th class="text-center">Action</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(value, key) in danhSachKhoDangNhap">
                                <th class="text-center align-middle">@{{ key + 1 }}</th>
                                <td class="align-middle text-nowrap">@{{ formatDate(value.created_at) }}</td>
                                <td class="align-middle text-nowrap">@{{ value.ten_san_pham }}</td>
                                <td class="align-middle" style="width: 50px">
                                    @{{ value.so_luong }}
                                </td>
                                <td class="align-middle">
                                    @{{ value.don_gia }}
                                </td>
                                <td class="align-middle">
                                    @{{ value.so_luong * value.don_gia }}
                                </td>

                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    new Vue({
        el  :   '#app',
        data:   {
            danhSachKhoDangNhap :   [],
        },
        created() {
            this.loadKho();
        },
        methods :   {
            loadKho() {
                axios
                    .get('/admin/nhap-kho/loadKho')
                    .then((res) => {
                        this.danhSachKhoDangNhap = res.data.dataKho;
                    });
            },
            formatDate(datetime)
            {
                const input = datetime;
                const dateObj = new Date(input);
                const year = dateObj.getFullYear();
                const month = (dateObj.getMonth()+1).toString().padStart(2, '0');
                const date = dateObj.getDate().toString().padStart(2, '0');

                const result = `${date}/${month}/${year}`;

                return result;
            },
        },
    });
</script>
@endsection
