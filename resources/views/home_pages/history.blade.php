@extends('home_pages.master')
@section('content')
<div class="container" id="app">
    <div class="row">
        <div class="col-lg-12">
            <div class="shoping__cart__table">
                <table>
                    <thead>
                        <tr>
                            <th class="shoping__product">#</th>
                            <th>Mã Đơn Hàng</th>
                            <th>Tổng Tiền</th>
                            <th>Ngày Mua</th>
                        </tr>
                    </thead>
                    <tbody>
                        <template v-for="(value, key) in listHoaDon" >
                            <tr>
                                <th>@{{ key + 1 }}</th>
                                <td class="shoping__cart__item text-center">
                                    <h5>@{{ value.ma_don_hang }}</h5>
                                </td>
                                <td class="shoping__cart__price">
                                    @{{ formatNumber(value.thuc_tra) }}
                                </td>
                                <td>
                                    @{{ formatDate(value.created_at) }}
                                </td>
                            </tr>
                        </template>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    new Vue({
        el      :   '#app',
        data    :   {
            listHoaDon      : [],
        },
        created() {
            this.loadCart();
        },
        methods :   {
            loadCart() {
                axios
                    .get('/hoa-don/data')
                    .then((res) => {
                        this.listHoaDon = res.data.data;
                    });
            },
            formatNumber(number) {
                return new Intl.NumberFormat('vi-VI', { style: 'currency', currency: 'VND' }).format(number);
            },

            formatDate(datetime)
            {
                const input = datetime;
                const dateObj = new Date(input);
                const year = dateObj.getFullYear();
                const month = (dateObj.getMonth()+1).toString().padStart(2, '0');
                const date = dateObj.getDate().toString().padStart(2, '0');

                const result = `${date}/${month}/${year}`;

                return result;
            },
        },
    });
</script>
@endsection
