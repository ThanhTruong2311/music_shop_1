@extends('home_pages.master')
@section('content')

    <section class="categories">
        <div class="container">
            <div class="row">
                <div class="categories__slider owl-carousel">
                    @foreach ($danhMucCha as $key => $value)
                        <div class="col-lg-3">
                            <div class="categories__item set-bg" data-setbg="/danh_muc/{{ $value->hinh_anh }}">
                                <h5><a href="/danh-muc/{{ $value->id }}">{{ $value->ten_danh_muc }}</a></h5>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <section class="featured spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Featured Product</h2>
                    </div>
                    <div class="featured__controls">
                        <ul>
                            <li class="active" data-filter="*">All</li>
                            @foreach ($danhMuc as $key => $value)
                                <li data-filter=".{{ $value->slug_danh_muc }}">{{ $value->ten_danh_muc }}</li>
                            @endforeach
                            {{-- <li data-filter=".fresh-meat">Fresh Meat</li>
                            <li data-filter=".vegetables">Vegetables</li>
                            <li data-filter=".fastfood">Fastfood</li> --}}
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row featured__filter">
                @foreach ($sanPham as $key => $value )
                <div class="col-lg-3 col-md-4 col-sm-6 mix oranges {{ $value->slug_danh_muc }}">
                    <div class="featured__item">
                        <div class="featured__item__pic set-bg" data-setbg="/san_pham/{{ $value->anh_dai_dien }}">
                            <ul class="featured__item__pic__hover">
                                <li><a><i class="fa fa-heart"></i></a></li>
                                <li><a><i class="fa fa-retweet"></i></a></li>
                                <li><a class="addToCart" data-id="{{ $value->id }}"><i class="fa fa-shopping-cart"></i></a></li>
                            </ul>
                        </div>
                        <div class="featured__item__text">
                            <h6><a href="/san-pham/{{ $value->id }}">{{ $value->ten_san_pham }}</a></h6>
                            <h5>{{ number_format($value->gia_khuyen_mai , 0) }} đ</h5>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

    <div class="banner mb-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="banner__pic">
                        <img src="/assets_homepage/img/banner/banner-1.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="banner__pic">
                        <img src="/assets_homepage/img/banner/banner-2.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
