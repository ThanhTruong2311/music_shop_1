@extends('home_pages.master')
@section('content')
<section class="shoping-cart spad">
    <div class="container" id="app">
        <div class="row">
            <div class="col-lg-12">
                <div class="shoping__cart__table">
                    <table id="tableCard">
                        <thead>
                            <tr>
                                <th class="shoping__product">Products</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Total</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <template v-for="(value, key) in listCart" >
                                <tr>
                                    <td class="shoping__cart__item">
                                        <h5>@{{ value.ten_san_pham }}</h5>
                                    </td>
                                    <td class="shoping__cart__price">
                                        @{{ formatNumber(value.don_gia) }}
                                    </td>
                                    <td class="shoping__cart__quantity">
                                        <div class="quantity">
                                            <div class="pro-qty">
                                                <input v-on:change="updateRow(value)" v-model="value.so_luong" type="number"/>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="shoping__cart__total">
                                        @{{ formatNumber(value.don_gia * value.so_luong)}}
                                    </td>
                                    <td class="shoping__cart__item__close">
                                        <span class="icon_close" v-on:click="deleteRow(value)"></span>
                                    </td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="shoping__checkout">
                    <h5>Cart Total</h5>
                    <ul>
                        <li>Subtotal <span>@{{ formatNumber(tong_tien) }}</span></li>
                        <li>Total <span>@{{ formatNumber(tong_tien) }}</span></li>
                    </ul>
                    <a class="primary-btn" v-on:click='thanhToan()'>THANH TOÁN</a>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="col-lg-12 col-md-12">
                    <div class="checkout__order">
                        <h4>Your Order</h4>
                        <div class="checkout__order__products">Products <span>Total</span></div>
                        <ul>
                            <template v-for="(value , index) in listSanPham">
                                <li>@{{ value.ten_san_pham }}<span>@{{ formatNumber(value.don_gia) }}</span></li>
                            </template>
                        </ul>
                        <div class="checkout__order__subtotal">Subtotal <span>@{{ formatNumber(tong_thanh_toan) }}</span></div>
                        <div class="checkout__order__total">Total <span>@{{ formatNumber(tong_thanh_toan) }}</span></div>
                        <div id="thanhtoan">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
    new Vue({
        el      :   '#app',
        data    :   {
            tong_tien           : 0,
            tong_thanh_toan     : 0,
            listCart            : [],
            listSanPham         : [],
        },
        created() {
            this.loadCart();
        },
        methods :   {
            loadCart() {
                axios
                    .get('/cart/data')
                    .then((res) => {
                        this.listCart = res.data.data;
                        this.tong_tien = 0;
                        this.listCart.forEach((item) => {
                            this.tongTien(item.so_luong * item.don_gia);
                        });

                    });
            },

            thanhToan(){
                const payload = {
                    'data'          : this.listCart,
                    'tong_tien'     : this.tong_tien,
                };
                var content = '<button class="btn btn-danger" >Đã Thanh Toán</button>';
                axios
                    .post('/thanh-toan' , payload)
                    .then((res) => {
                        if(res.data.status){
                            this.listSanPham = res.data.data.data;
                            this.listCart.forEach((item) => {
                                this.tongThanhToan(item.so_luong * item.don_gia);
                            });
                            $("#thanhtoan").html(content);
                            $("#tableCard").hide();
                            toastr.success("Đã thanh toán thành công");
                        }
                    });
            },
            tongTien(value)
            {
                this.tong_tien =  this.tong_tien + value * 1;

            },

            tongThanhToan(value)
            {
                this.tong_thanh_toan =  this.tong_thanh_toan + value * 1;

            },
            formatNumber(number) {
                return new Intl.NumberFormat('vi-VI', { style: 'currency', currency: 'VND' }).format(number);
            },
            updateRow(row) {
                axios
                    .post('/add-to-cart-update', row)
                    .then((res) => {
                        if(res.status) {
                            toastr.success("Đã cập nhật giỏ hàng!");
                            this.loadCart();
                        }
                    });
            },
            deleteRow(row) {
                axios
                    .post('/remove-cart', row)
                    .then((res) => {
                        toastr.success("Đã cập nhật giỏ hàng!");
                        this.loadCart();
                    });
            },
        },
    });
</script>
@endsection
