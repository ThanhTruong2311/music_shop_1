@extends('home_pages.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-5">
            <div class="sidebar">
                <div class="sidebar__item">
                    <h4>Department</h4>
                    <ul>
                        @foreach ($menuCon as $key => $value)
                            <li><a href="/danh-muc/{{ $value->id }}">{{ $value->ten_danh_muc }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-md-7">
            <div class="product__discount">
                <div class="section-title product__discount__title">
                    <h2>Product</h2>
                </div>
                <div class="row">
                    @foreach ($sanPham as $key => $value)
                        <div class="col-lg-4">
                            <div class="product__discount__item">
                                <div class="product__discount__item__pic set-bg"
                                    data-setbg="/san_pham/{{ $value->anh_dai_dien }}">
                                    <div class="product__discount__percent">{{number_format(($value->gia_ban - $value->gia_khuyen_mai) / $value->gia_ban , 2)  }}%</div>
                                    <ul class="product__item__pic__hover">
                                        <li><a><i class="fa fa-heart"></i></a></li>
                                        <li><a><i class="fa fa-retweet"></i></a></li>
                                        <li><a class="addToCart" data-id="{{ $value->id }}"><i class="fa fa-shopping-cart"></i></a></li>
                                    </ul>
                                </div>
                                <div class="product__discount__item__text">
                                    <span>{{ $value->ten_danh_muc }}</span>
                                    <h5><a href="/san-pham/{{ $value->id }}"></a>{{ $value->ten_san_pham }}</h5>
                                    <div class="product__item__price">{{ number_format($value->gia_khuyen_mai , 0) }} đ<span>{{ number_format($value->gia_ban, 0) }} đ</span></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
