<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddToCartRequest;
use App\Models\ChiTietDonHang;
use App\Models\DonHang;
use App\Models\SanPham;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChiTietDonHangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home_pages.cart');
    }

    public function addToCart(AddToCartRequest $request)
    {


        // Phải kiểm tra xem là đã login hay chưa?
        $agent = Auth::guard('agent')->user();
        if($agent) {
            $sanPham = SanPham::find($request->san_pham_id);

            $chiTietDonHang = ChiTietDonHang::where('san_pham_id', $request->san_pham_id)
                                            ->where('is_cart', 1)
                                            ->whereNull('don_hang_id')
                                            ->where('agent_id', $agent->id)
                                            ->first();
            if($chiTietDonHang) {
                $chiTietDonHang->so_luong += $request->so_luong;
                $chiTietDonHang->save();
            } else {
                ChiTietDonHang::create([
                    'san_pham_id'       => $sanPham->id,
                    'ten_san_pham'      => $sanPham->ten_san_pham,
                    'don_gia'           => $sanPham->gia_khuyen_mai ? $sanPham->gia_khuyen_mai : $sanPham->gia_ban,
                    'so_luong'          => $request->so_luong,
                    'is_cart'           => 1,
                    'agent_id'          => $agent->id,
                ]);
            }
            return response()->json(['status' => true]);
        } else {
            return response()->json(['status' => false]);
        }
    }

    public function addToCartUpdate(AddToCartRequest $request)
    {
        // Phải kiểm tra xem là đã login hay chưa?
        $agent = Auth::guard('agent')->user();
        if($agent) {
            $sanPham = SanPham::find($request->san_pham_id);

            $chiTietDonHang = ChiTietDonHang::where('san_pham_id', $request->san_pham_id)
                                            ->where('is_cart', 1)
                                            ->where('agent_id', $agent->id)
                                            ->first();
            if($chiTietDonHang) {
                $chiTietDonHang->so_luong = $request->so_luong;
                $chiTietDonHang->save();
            } else {
                ChiTietDonHang::create([
                    'san_pham_id'       => $sanPham->id,
                    'ten_san_pham'      => $sanPham->ten_san_pham,
                    'don_gia'           => $sanPham->gia_khuyen_mai ? $sanPham->gia_khuyen_mai : $sanPham->gia_ban,
                    'so_luong'          => $request->so_luong,
                    'is_cart'           => 1,
                    'agent_id'          => $agent->id,
                ]);
            }
            return response()->json(['status' => true]);
        } else {
            return response()->json(['status' => false]);
        }
    }

    public function removeCart(Request $request)
    {
        $agent = Auth::guard('agent')->user();
        if($agent) {
            $chiTietDonHang = ChiTietDonHang::where('is_cart', 1)
                                            ->where('agent_id', $agent->id)
                                            ->where('san_pham_id', $request->san_pham_id)
                                            ->first();
            $chiTietDonHang->delete();
        }
    }

    public function thanhToan(Request $request)
    {
        $agent = Auth::guard('agent')->user()->id;
        $data = $request->all();
        $donhang = DonHang::create([
            'ma_don_hang'       => "RANDOM" ,
            'tong_tien'         => $request->tong_tien ,
            'tien_giam_gia'     => $request->tong_tien ,
            'thuc_tra'          => $request->tong_tien ,
            'agent_id'          => $agent,
            'loai_thanh_toan'   => 1,
        ]);
        foreach ($data['data'] as $key => $value) {
            $sanPham = ChiTietDonHang::find($value['id']);
            $sanPham->don_hang_id = $donhang->id;
            $sanPham->save();
        }
        $donhang->ma_don_hang = "HDTT" . 1000000000 + $donhang->id;
        $donhang->save();
        return response()->json([
            'status' => true,
            'data'   => $data,
        ]);
    }

    public function dataCart()
    {
        $agent = Auth::guard('agent')->user();
        if($agent) {
            $data = ChiTietDonHang::join('san_phams', 'chi_tiet_don_hangs.san_pham_id', 'san_phams.id')
                                  ->where('agent_id', $agent->id)
                                  ->whereNull('don_hang_id')
                                  ->where('is_cart', 1)
                                  ->select('chi_tiet_don_hangs.*', 'san_phams.anh_dai_dien')
                                  ->get();
            return response()->json(['data' => $data]);
        }
    }

    public function getHoaDon()
    {
        $agent = Auth::guard('agent')->user();
        if($agent){
            $data = DonHang::where('agent_id' , $agent->id)
                            ->orderBy('created_at' , 'DESC')
                            ->get();

            return response()->json([
                'data' => $data,
            ]);
        }
    }

    public function listHoaDon()
    {
        return view('home_pages.history');
    }


}
