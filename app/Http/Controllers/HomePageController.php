<?php

namespace App\Http\Controllers;

use App\Models\Config;
use App\Models\DanhMucSanPham;
use App\Models\SanPham;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class HomePageController extends Controller
{
    public function index()
    {
        $danhMucCha = DanhMucSanPham::where('is_open' , 1)
                                    ->where('id_danh_muc_cha' ,0)
                                    ->get();
        $danhMuc = DanhMucSanPham::where('is_open' , 1)
                                    ->where('id_danh_muc_cha' , '<>' , 0)
                                    ->get();
        $sanPham = SanPham::join('danh_muc_san_phams' , 'danh_muc_san_phams.id' , 'san_phams.id_danh_muc')
                                    ->where('san_phams.is_open' , 1)
                                    ->select('san_phams.*', 'danh_muc_san_phams.slug_danh_muc as slug_danh_muc')
                                    ->get();
        return view('home_pages.home' , compact('danhMucCha', 'danhMuc', 'sanPham'));
    }

    public function viewSanPham($id)
    {
        // while(strpos($id, 'post')) {
        //     $viTri = strpos($id, 'post');
        //     $id = Str::substr($id, $viTri + 4);
        // }

        $sanPham = SanPham::find($id);

        if($sanPham) {
            return view('home_pages.product_detail', compact('sanPham'));
        } else {
            return redirect('/');
        }
    }

    public function viewDanhMuc($id)
    {

        while(strpos($id, 'post')) {
            $viTri = strpos($id, 'post');
            $id = Str::substr($id, $viTri + 4);
        }

        $danhMuc = DanhMucSanPham::find($id);
        if($danhMuc) {
            // Nếu là danh mục con
            if($danhMuc->id_danh_muc_cha > 0) {
                $sanPham = SanPham::where('is_open', 1)
                                  ->where('id_danh_muc', $danhMuc->id)
                                  ->get();
            } else {
                // Nó là danh mục cha. Tìm toàn bộ danh mục con
                $danhMucCon = DanhMucSanPham::where('id_danh_muc_cha', $danhMuc->id)
                                            ->get();
                $danhSach   = $danhMuc->id;
                foreach($danhMucCon as $key => $value) {
                    $danhSach = $danhSach . ',' . $value->id;
                }
                $sanPham = SanPham::join('danh_muc_san_phams' , 'danh_muc_san_phams.id' , 'san_phams.id_danh_muc')
                                    ->whereIn('san_phams.id_danh_muc', explode(",", $danhSach))
                                    ->select('san_phams.*' , 'danh_muc_san_phams.ten_danh_muc as ten_danh_muc')
                                    ->get();
            }

            return view('home_pages.product', compact('sanPham'));
        }
    }
}
